var hbs = require('express-handlebars');
var express = require('express');
var path = require('path');

var routes = require('./routes/index.');

var app = express();


app.engine('hbs' , hbs({extname: 'hbs' , defaultLayout: 'layout' , layoutsDir: __dirname + '/layout/'}));
app.set('views' , path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
